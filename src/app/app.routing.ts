import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home.component';

const APP_ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'recipes', loadChildren: 'app/recipe/recipe.module#RecipeModule'},
  {path: 'shopping-list', loadChildren: 'app/shopping-list/shopping-list.module#ShoppingListModule'}
];

export const routing = RouterModule.forRoot(APP_ROUTES);
