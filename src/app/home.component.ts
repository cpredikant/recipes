import {Component} from '@angular/core';

@Component({
  selector: 'app-home',
  template: '<h1>Hello!</h1>'
})
export class HomeComponent{}
