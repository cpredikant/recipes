import {EventEmitter, Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Recipe} from './recipe.model';


@Injectable()
export class RecipeService {

  recipesChanged = new EventEmitter<Recipe[]>();

  private recipes: Recipe[] = [];

  constructor(private http: HttpClient) {

  }


  getRecipes() {
    return this.recipes;
  }

  getRecipe(id: number) {
    return this.recipes[id];
  }

  deleteRecipe(id: number) {
    this.recipes.splice(id, 1);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
  }

  editRecipe(oldRecipe: Recipe, newRecipe: Recipe) {
    this.recipes[this.recipes.indexOf(oldRecipe)] = newRecipe;
  }

  storeData() {
    const body = JSON.stringify(this.recipes);
    const httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.put('https://cp-recipe-app.firebaseio.com/recipe.json', body, {headers: httpHeaders});
  }

  fetchData() {
    this.http.get<Recipe[]>('https://cp-recipe-app.firebaseio.com/recipe.json').subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
        this.recipesChanged.emit(this.recipes);
      });
  }

}
