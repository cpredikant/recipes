import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Ingredient} from "../shared/ingredient.model";
import {ShoppingListService} from "./shopping-list.service";

@Component({
  selector: 'app-shopping-list-add',
  templateUrl: './shopping-list-add.component.html',
  styles: []
})
export class ShoppingListAddComponent implements OnInit, OnChanges {

  @Input() selectedIngredient: Ingredient;
  @Output() cleared = new EventEmitter();

  isAdd = true;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
  }

  ngOnChanges(changes){
    if(changes.selectedIngredient.currentValue == null){
      this.selectedIngredient = {name: null, amount: null};
      this.isAdd = true;
    } else {
      this.isAdd = false;

    }
  }

  onSubmit(form: NgForm){
    const newIngredient = new Ingredient(form.value.name, form.value.amount);

    if (this.isAdd) {
      this.shoppingListService.addIngredient(newIngredient);
    } else {
      this.shoppingListService.editIngredient(this.selectedIngredient, newIngredient);
    }

    this.onReset(form);
  }

  onReset(form: NgForm){
    this.cleared.emit();
    form.resetForm();
  }

  onDelete(form: NgForm){
    this.shoppingListService.deleteIngredient(this.selectedIngredient);
    this.onReset(form);
  }

}
